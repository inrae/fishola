---
-- #%L
-- Fishola :: Backend
-- %%
-- Copyright (C) 2019 - 2020 INRAE - UMR CARRTEL
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
insert into fishola_user (first_name, last_name, email, password) values
    ('Arnaud', 'Thimel', 'thimel@codelutin.com', '$2a$12$NSO7uY1VZwBeEc08FTtdUef3OpPQv0lDDZwRLHcPvl2dOtNDZ8OBu');
insert into fishola_user (first_name, last_name, email, password) values
    ('Chloé', 'Goulon', 'chloe.goulon@inrae.fr', '$2a$12$3NOX6pT.yf5eZH9rUXSkxOr.7.afF2nC52C4iMf.HFg6YV9VgeM2i');
insert into fishola_user (first_name, last_name, email, password) values
    ('Antoine', 'SCHELLENBERGER', 'antoine.schellenberger@inrae.fr', '$2a$12$yeCouM2gW6mbkaQj2OXgJ.oyGu.EuC84vMz6lrthPxuiVSx58LOwO');

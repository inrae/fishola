---
-- #%L
-- Fishola :: Backend
-- %%
-- Copyright (C) 2019 - 2021 INRAE - UMR CARRTEL
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---

ALTER TABLE documentation
  ADD COLUMN news BOOLEAN;

UPDATE documentation
  SET news = FALSE;

ALTER TABLE documentation
  ALTER COLUMN news
    SET NOT NULL;

COMMENT ON COLUMN documentation.news IS 'Permet de savoir s''il s''agit d''une actualité ou non';

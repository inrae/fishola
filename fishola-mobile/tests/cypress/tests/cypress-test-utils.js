/*-
 * #%L
 * Fishola :: Mobile
 * %%
 * Copyright (C) 2019 - 2021 INRAE - UMR CARRTEL
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import MarkerTestPicture from "../../commons/MarkerTestPicture";

const defaultMarkerPath = "markers/marker.jpg";

export function markerPic(imgPath, comment, expectedMarkerRatio) {
  return new MarkerTestPicture(
    defaultMarkerPath,
    "markers/" + imgPath,
    true,
    expectedMarkerRatio,
    comment,
    3
  );
}

export function fishWithMarkerPic(
  imgPath,
  comment,
  expectedFishOnImageRatio,
  picQuality,
  expectedMeasureInMm
) {
  return new MarkerTestPicture(
    defaultMarkerPath,
    "markers/" + imgPath,
    true,
    expectedFishOnImageRatio,
    comment,
    picQuality,
    expectedMeasureInMm
  );
}

export function fishPic(
  imgPath,
  comment,
  expectedFishOnImageRatio,
  picQuality
) {
  return new MarkerTestPicture(
    defaultMarkerPath,
    "fishes/test_" + imgPath,
    false,
    expectedFishOnImageRatio,
    comment,
    picQuality
  );
}

/*-
 * #%L
 * Aires Éducatives
 * %%
 * Copyright (C) 2020 OFB
 * %%
 * OFB - Tous droits réservés
 * #L%
 */
module.exports = {
  preset: "@vue/cli-plugin-unit-jest/presets/typescript-and-babel"
}

# Module admin de Fishola

## Démarrer en mode dev

Il suffit de lancer la commande suivante :

```bash
npm run serve
```

L'application tourne sur le port `8082` : [http://localhost:8082](http://localhost:8082).

